# python-unknown-licenses-example



## SUMMARY

This is a simple python project where dependency scan runs but some packages are showing up with an unknown license.

The project has been created to reproduce this bug: https://gitlab.com/gitlab-org/gitlab/-/issues/470151